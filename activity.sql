-- A.
SELECT * FROM artists WHERE name LIKE "%d%" OR "d%" OR "%d";

-- B.
SELECT * FROM songs WHERE length < 230;

-- C.
SELECT album_title, song_name, length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- D.
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    WHERE album_title LIKE "%a%" OR "a%" OR "%a";

-- E.
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- F.
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY album_title DESC, song_name ASC;