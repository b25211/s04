-- [SECTION] Advance Selects
-- To exclude records, use '!' which is similar to a NOT operator

SELECT * FROM songs WHERE id != 11;
SELECT * FROM songs WHERE id > 11;
SELECT * FROM songs WHERE id < 11;

-- OR Operator
-- Use the OR operator when querying for specific records with specific column values
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

-- IN Operator
SELECT * FROM songs WHERE id IN (15,16,17);
SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");

-- Combining conditions
SELECT * FROM songs WHERE  album_id = 4 AND id > 8;

-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "%a"; -- last words with letter 'a'
SELECT * FROM songs WHERE song_name LIKE "a%"; -- first words with letter 'a'
SELECT * FROM songs WHERE song_name LIKE "%a%"; -- all words with letter 'a'

-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- [SECTION] Table Joins
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;

-- Since we're joining tables, it means that we can access any property/column that exists in either of the tables in one single SQL query
SELECT artists.name, albums.album_title FROM artists
    JOIN albums ON artists.id = albums.artist_id;

